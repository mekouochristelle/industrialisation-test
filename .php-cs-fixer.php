<?php

$finder = PhpCsFixer\Finder::create()
 ->in(__DIR__)
 ->exclude('vendor');


return PhpCsFixer\Config::create()
 ->setRules([
 // Définissez ici les règles que vous souhaitez appliquer
         'align_multiline_comment' => false,
         'array_indentation' => false,
         'array_push' => false,
         'array_syntax' => false,
         'no_unused_imports' => true,])
 ->setFinder($finder);
